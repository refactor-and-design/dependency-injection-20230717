package com.thoughtworks.design;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StarterTest {
    @Test
    public void should_bind_instance_to_class(){
        ContextConfig contextConfig = new ContextConfig();
        Component instance = new Component(){};
        contextConfig.bind(Component.class, instance);

        Context context = contextConfig.buildContext();
        Component actual = context.get(Component.class);

        assertSame(instance, actual);
    }

    @Test
    public void should_bind_class_with_default_constructor(){
        ContextConfig contextConfig = new ContextConfig();
        contextConfig.bind(Component.class, ComponentImplement.class);

        Context context = contextConfig.buildContext();
        Component actual = context.get(Component.class);

        assertNotNull(actual);
    }

    @Test
    public void should_bind_class_with_inject_constructor() {
        ContextConfig contextConfig = new ContextConfig();

        String expect = "expect";
        contextConfig.bind(String.class, expect);
        contextConfig.bind(Component.class, ComponentImplementWithDependency.class);
        Context context = contextConfig.buildContext();
        Component actual = context.get(Component.class);

        assertNotNull(actual);
        String name = ((ComponentImplementWithDependency) actual).getName();
        assertSame(name, expect);
    }
    @Test
    public void should_throw_exception_if_dependency_not_found(){
        ContextConfig contextConfig = new ContextConfig();

        contextConfig.bind(Component.class, ComponentImplementWithDependency.class);

        assertThrows(DependencyNotFoundException.class, () -> {
            Context context = contextConfig.buildContext();
            context.get(Component.class);
        });
    }
    @Test
    public void should_throw_exception_if_cyclic_dependency_be_found(){
        ContextConfig contextConfig = new ContextConfig();

        contextConfig.bind(Dependency.class, DependencyImplement.class);
        contextConfig.bind(Component.class, Implement.class);

        assertThrows(CyclicDependencyFoundException.class, () -> {
            contextConfig.buildContext();
        });
    }

    @Test
    public void should_bind_class_by_inject_field(){
        ContextConfig contextConfig = new ContextConfig();
        String expect = "expect";
        contextConfig.bind(String.class, expect);
        contextConfig.bind(Component.class, ImplementWithInjectField.class);

        Context context = contextConfig.buildContext();

        Component component = context.get(Component.class);
        assertNotNull(component);
        ImplementWithInjectField actual = (ImplementWithInjectField) component;
        assertSame(expect ,actual.getName());
    }
}

interface Dependency {}

class DependencyImplement implements Dependency {
    Component component;

    @Inject
    public DependencyImplement(Component component) {
        this.component = component;
    }
}

class Implement implements Component {
    Dependency dependency;

    @Inject
    public Implement(Dependency dependency) {
        this.dependency = dependency;
    }
}
class ComponentImplementWithDependency implements Component {
    String name;
    @Inject
    public ComponentImplementWithDependency(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

interface Component{}
class ComponentImplement implements Component{
    public ComponentImplement(){}
}

class ImplementWithInjectField implements Component {
    @Inject
    String name;

    public ImplementWithInjectField() {
    }

    public String getName() {
        return name;
    }
}


//interface Dependency{}
//class DependencyImplementA implements Dependency{}
//class DependencyImplementB implements Dependency{}
//
//interface DemoInterface {}
//@Bean(DemoInterface.class)
//class Demo implements DemoInterface{
//    Dependency name;
//    @Inject
//    public Demo(Dependency name){
//        this.name = name;
//    }
//}


