package com.thoughtworks.design;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class TypeProvider<Type> implements Provider<Type> {
    private final Class<Type> componentImplementClass;
    private Constructor<?> constructor;

    public TypeProvider(Class<Type> componentImplementClass) {
        this.componentImplementClass = componentImplementClass;
        constructor = getConstructor();
    }

    @Override
    public Type getInstance(Context context) {
        try {
            Object[] parameters = getParameters(constructor, context);
            return (Type) constructor.newInstance(parameters);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<?> getDependencies() {
        return (List<Type>) Arrays.stream(constructor.getParameters())
                .map(p -> p.getType()).collect(Collectors.toList());
    }

    private Object[] getParameters(Constructor<?> constructor, Context context){
        Parameter[] parameters = constructor.getParameters();
        Object[] objects = Arrays.stream(parameters).map(parameter -> {
            Class<?> componentClass = parameter.getType();
            return context.get(componentClass);
        }).toArray();

        return objects;
    }
    private Constructor<?> getConstructor() {
        Constructor<?> defaultConstructor = Arrays.stream(componentImplementClass.getConstructors()).findFirst().get();
        return Arrays.stream(componentImplementClass.getConstructors())
                .filter(constructor -> constructor.isAnnotationPresent(Inject.class)).findFirst()
                .orElse(defaultConstructor);
    }

}
