package com.thoughtworks.design;

import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class ContextConfig {
    HashMap<Class<?>, Provider<?>> providers = new HashMap<>();
//    HashMap<Class<?>, TypeProvider<?>> providers = new HashMap<>();

    //终止条件
    //寻找下一个
    //调用自己
    private void checkCylciDependency(Class<?> current, Stack<Class> checking) {
        // 曾经被检查过; 我检查自己是不是循环依赖
        //结束条件
        if (checking.contains(current)){
            throw new CyclicDependencyFoundException();
        }
        checking.push(current);
        //下一个
        List<?> dependencies = providers.get(current).getDependencies();
        //调用本函数
        // 检查我所有的依赖是否存在循环依赖
        dependencies.forEach(dependency -> checkCylciDependency((Class<?>) dependency, checking));
        checking.pop();
    }

    public <T> void bind(Class<T> componentClass, T instance) {
        providers.put(componentClass, new InstanceProvider<>(instance));
    }

    public <Type, Implement extends Type> void bind(Class<Type> componentClass,
            Class<Implement> componentImplementClass) {
        providers.put(componentClass, new TypeProvider<Type>((Class<Type>) componentImplementClass));
    }

    public Context buildContext(){
        providers.keySet().stream().forEach(key -> {
            Provider<?> provider = providers.get(key);
            List<?> dependencies = provider.getDependencies();
            if(dependencies.stream().anyMatch(dependency -> !providers.containsKey(dependency))){
                throw new DependencyNotFoundException();
            }
        });

        providers.keySet().forEach(provider -> {
            checkCylciDependency(provider, new Stack<>());
        });

        return new Context(providers);
    }

}
