package com.thoughtworks.design;

import java.util.HashMap;

public class Context {
    HashMap<Class<?>, Provider<?>> providers = new HashMap<>();

    public Context(HashMap<Class<?>, Provider<?>> providers) {
        this.providers = providers;
    }

    public <T> T get(Class<T> componentClass) {
        if (!providers.containsKey(componentClass)){
            throw new DependencyNotFoundException();
        }
        return (T) providers.get(componentClass).getInstance(this);
    }
}
