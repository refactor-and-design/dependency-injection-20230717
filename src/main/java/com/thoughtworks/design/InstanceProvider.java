package com.thoughtworks.design;

import java.util.List;

class InstanceProvider<T> implements Provider<T> {
    private final T instance;

    public InstanceProvider(T instance) {
        this.instance = instance;
    }

    @Override
    public T getInstance(Context context) {
        return instance;
    }

    @Override
    public List<T> getDependencies() {
        return List.of();
    }
}
