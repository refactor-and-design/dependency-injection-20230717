package com.thoughtworks.design;

import java.util.List;

public interface Provider<T> {
    T getInstance(Context context);

    List<?> getDependencies();
}
